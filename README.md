# Ng2AsyncComponent

You will have to have `two` terminal instances running, one for the angular development server and one for the views server.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## View server
Open a new terminal instance. Run `npm run views` for a views server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change the `views.json` file.

## The Dynamic Component service
Here is where all the magic happens `src\app\comploader.service.ts`

## Known Issues
When using dynamic templating AOT cannot be used. We would need to run AOT separately for components and have templates rendered with JIT

Passing data would have to be explicitly done with services