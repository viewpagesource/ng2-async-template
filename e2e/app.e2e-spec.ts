import { BjsAcynComponentPage } from './app.po';

describe('bjs-acyn-component App', function() {
  let page: BjsAcynComponentPage;

  beforeEach(() => {
    page = new BjsAcynComponentPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
