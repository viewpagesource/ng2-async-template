/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ComploaderService } from './comploader.service';

describe('ComploaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComploaderService]
    });
  });

  it('should ...', inject([ComploaderService], (service: ComploaderService) => {
    expect(service).toBeTruthy();
  }));
});
