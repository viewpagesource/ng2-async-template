import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PdpComponent } from '../pdp/pdp.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PdpComponent
  ],
  entryComponents:[PdpComponent],
  exports:[PdpComponent]
})
export class TemplateModule { }
