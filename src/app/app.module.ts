import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { TemplateModule } from './template/template.module';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import {ComploaderService} from './comploader.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TemplateModule,
    ComponentsModule
  ],
  providers: [ComploaderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
