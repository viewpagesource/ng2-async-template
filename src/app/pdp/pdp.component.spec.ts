/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PdpComponent } from './pdp.component';

describe('PdpComponent', () => {
  let component: PdpComponent;
  let fixture: ComponentFixture<PdpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
