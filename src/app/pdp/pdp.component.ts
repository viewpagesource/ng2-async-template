import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ComploaderService} from '../comploader.service';


@Component({
  selector: 'bjs-pdp',
  template: `
    <template #pdpContainer></template>
  `,
  styleUrls: ['./pdp.component.css']
})
export class PdpComponent implements OnInit {
  componentType:string = 'pdp';
  @ViewChild('pdpContainer', { read: ViewContainerRef }) container: ViewContainerRef;
  constructor(private componentLoaderService: ComploaderService) { }
  ngOnInit():Promise<any> {
    return this.getAsyncTemplate();
  }
  private getAsyncTemplate():Promise<any>{
    console.log(this);
    return this.componentLoaderService.getAsyncTemplate(this.componentType,this.container);
  }
}
