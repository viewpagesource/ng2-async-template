import {Compiler, NgModule, Component, ViewContainerRef, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ComponentsModule } from './components/components.module';
import { TemplateModule } from './template/template.module';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ComploaderService {

  constructor(private compiler:Compiler,private http:Http) { }
  createComponent(template:string, container:ViewContainerRef):any{
    @Component({
      template:template
    })
    class TemplateComponent {}

    @NgModule({
      imports:[ComponentsModule, TemplateModule],
      declarations: [TemplateComponent]
    })
    class DynamicTemplateModule {}

    this.compiler.compileModuleAndAllComponentsAsync(DynamicTemplateModule).then( mod => {   
      const factory = mod.componentFactories.find((comp) =>
        comp.componentType === TemplateComponent
      );
      container.createComponent(factory);
    });
  }
  getAsyncTemplate(componentType:string, container:ViewContainerRef):Promise<any> {
    const url = `http://localhost:3000/api/${componentType}`;
    return this.http.get(url)
            .toPromise()
            .then(response => {
              this.createComponent(response.json()[0]['field_'+ componentType ], container);
            })
            .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
