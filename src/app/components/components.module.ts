import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PdpHelloComponent } from '../pdp-hello/hello.component';
import { PdpHeaderComponent } from '../pdp-header/pdp-header.component';
import { PdpFooterComponent } from '../pdp-footer/pdp-footer.component';


const componentsArr = [
  PdpHelloComponent,
  PdpHeaderComponent,
  PdpFooterComponent
]

@NgModule({
  imports: [
    CommonModule
  ],
  declarations:componentsArr,
  entryComponents:componentsArr,
  exports:componentsArr
})
export class ComponentsModule { }
