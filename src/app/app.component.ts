// import { Component } from '@angular/core';

// @Component({
//   selector: 'bjs-root',
//   templateUrl: './app.component.html',
//   styleUrls: ['./app.component.css']
// })
// export class AppComponent {
//   title = 'app works!';
// }


import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ComploaderService} from './comploader.service';


@Component({
  selector: 'bjs-root',
  template: `
    <template #rootContainer></template>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  componentType:string = 'root';
  @ViewChild('rootContainer', { read: ViewContainerRef }) container: ViewContainerRef;
  constructor(private componentLoaderService: ComploaderService) { }
  ngOnInit():Promise<any> {
    return this.getAsyncTemplate();
  }
  private getAsyncTemplate():Promise<any>{
    console.log(this);
    return this.componentLoaderService.getAsyncTemplate(this.componentType,this.container);
  }
}
